from django.urls import path

from languages.views import CreateLanguageView

urlpatterns = [
    path('create_language', CreateLanguageView.as_view(), name='create_language'),
]
