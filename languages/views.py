from languages.models import Language
from languages.forms import CreateLanguageForm
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin


class CreateLanguageView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Language
    form_class = CreateLanguageForm

    def test_func(self):
        account = self.request.user.account
        teaching_courses = account.teaching_courses.all()
        assistant_courses = account.assistant_courses.all()
        return (teaching_courses | assistant_courses).exists()
