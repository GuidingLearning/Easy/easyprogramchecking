from django.db import models

# Create your models here.

class Language(models.Model):
    name = models.CharField(max_length=15, blank=False)
    compile_command = models.CharField(max_length=100, blank=True)
    run_command = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return '/'
