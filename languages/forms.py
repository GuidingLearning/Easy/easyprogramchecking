from django import forms
from languages.models import Language

class CreateLanguageForm(forms.ModelForm):
    class Meta:
        model = Language
        fields = ['name', 'compile_command', 'run_command']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['compile_command'].widget.attrs['class'] = 'form-control'
        self.fields['run_command'].widget.attrs['class'] = 'form-control'

