from django.db import models
from users.models import Account
from assignments.models import Assignment


class FeedbackGroup(models.Model):
    user = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    assignment = models.ForeignKey(Assignment, null=False, on_delete=models.CASCADE)
    done = models.BooleanField(default=False, blank=False)
    error = models.CharField(max_length=200, default="", blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.__str__() + ":" + str(self.id)

    def save_as_csv(self):
        pass


class Feedback(models.Model):
    name = models.CharField(max_length=200, default="", null=True, blank=True)
    group = models.ForeignKey(FeedbackGroup, default=None, null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    assignment = models.ForeignKey(Assignment, null=False, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user) + ": " + str(self.assignment)


class ErrorFeedback(models.Model):
    name = models.CharField(max_length=200, default="", null=True, blank=True)
    group = models.ForeignKey(FeedbackGroup, default=None, null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Account, null=True, on_delete=models.CASCADE)
    assignment = models.ForeignKey(Assignment, null=False, on_delete=models.CASCADE)
    error = models.CharField(max_length=500)


class SingleTestFeedback(models.Model):
    input = models.CharField(max_length=500)
    actual_output = models.CharField(max_length=500, default="")
    expected_output = models.CharField(max_length=500)
    error = models.CharField(max_length=500)
    comment = models.CharField(max_length=500)
    passed = models.BooleanField()
    feedback = models.ForeignKey(Feedback, null=False, on_delete=models.CASCADE)

    class Meta:
        abstract = True

    def show_complete_feedback(self):
        pass

    def __str__(self):
        return str(self.feedback) + ": " + str(self.id)


class PublicTestFeedback(SingleTestFeedback):

    def show_feedback(self):
        return self.show_complete_feedback()


class SemiPrivateTestFeedback(SingleTestFeedback):

    def show_feedback(self):
        # show message and passed_state
        pass


class PrivateTestFeedback(SingleTestFeedback):

    def show_feedback(self):
        return ''
