from django.contrib import admin
from feedback.models import FeedbackGroup, ErrorFeedback, Feedback, PublicTestFeedback, SemiPrivateTestFeedback, PrivateTestFeedback

# Register your models here.
admin.site.register(FeedbackGroup)
admin.site.register(Feedback)
admin.site.register(ErrorFeedback)
admin.site.register(PublicTestFeedback)
admin.site.register(SemiPrivateTestFeedback)
admin.site.register(PrivateTestFeedback)