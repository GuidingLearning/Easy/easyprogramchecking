from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect, HttpResponse
from users.models import Account
from django.core.files.temp import NamedTemporaryFile
from feedback.models import Feedback, FeedbackGroup, PrivateTestFeedback, PublicTestFeedback, SemiPrivateTestFeedback

import csv


class ViewFeedback(TemplateView):
    template_name = "view_feedback.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['title'] = "My Feedback"

        user = self.request.user

        context['user'] = user
        account = Account.objects.get(pk=user.id)
        groups = reversed(FeedbackGroup.objects.filter(user=account))

        context['feedback_groups'] = groups
        return context


def generate_verbose_table(name, private_feedback, public_feedback, semiprivate_feedback):
    with open(name, 'w', newline="") as myfile:
        filewriter = csv.writer(myfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Filename', 'Successful', 'Input', 'Expected Output', 'Output', 'Comment', 'Test type'])

        for f in private_feedback:
            filewriter.writerow(
                [f.feedback.name, f.passed, f.input, f.expected_output, f.actual_output, f.comment, "private"])
        for f in public_feedback:
            filewriter.writerow(
                [f.feedback.name, f.passed, f.input, f.expected_output, f.actual_output, f.comment, "public"])
        for f in semiprivate_feedback:
            filewriter.writerow(
                [f.feedback.name, f.passed, f.input, f.expected_output, f.actual_output, f.comment, "semi-private"])


# user x test result
def get_filenames(private_feedback, public_feedback, semiprivate_feedback):
    names = {}
    for f in private_feedback:
        names[f.feedback.name] = f.feedback.name
    for f in public_feedback:
        names[f.feedback.name] = f.feedback.name
    for f in semiprivate_feedback:
        names[f.feedback.name] = f.feedback.name
    return [name for name in names]


def get_tests_by_name(filenames_list, feedback):
    f_by_name = {name: [] for name in filenames_list}

    for f in feedback:
        f_by_name[f.feedback.name] += [f.passed]
    return f_by_name


def proccess(private_feedback, public_feedback, semiprivate_feedback):
    filenames_list = get_filenames(private_feedback, public_feedback, semiprivate_feedback)

    public_dict = get_tests_by_name(filenames_list, public_feedback)
    semiprivate_feedback = get_tests_by_name(filenames_list, semiprivate_feedback)
    private_dict = get_tests_by_name(filenames_list, private_feedback)

    return filenames_list, private_dict, public_dict, semiprivate_feedback


def generate_summary_table(name, private_feedback, public_feedback, semiprivate_feedback):
    with open(name, 'w', newline="") as myfile:
        filewriter = csv.writer(myfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        filenames, priv_test_dict, public_test_dict, semiprivate_test_dict = proccess(private_feedback, public_feedback,
                                                                                      semiprivate_feedback)

        tot_tests = len(priv_test_dict) + len(public_test_dict) + +len(semiprivate_test_dict)
        first_row = ['Filename'] + ["Test " + str(i + 1) for i in range(tot_tests + 1)] + ['Lex']
        filewriter.writerow(first_row)

        tests_dict = {name: (public_test_dict[name] + semiprivate_test_dict[name] + priv_test_dict[name]) for name in
                      filenames}
        for filename in tests_dict:
            lex = "'"
            for test in tests_dict[filename]:
                if test == True:
                    lex+="1"
                else:
                    lex+="0"
            lex+="'"
            filewriter.writerow([filename] + tests_dict[filename]+[lex])


def feedback_group_as_csv(request, generate_table=generate_verbose_table):
    code = request.GET['code']
    name = 'FeedbackGroup' + request.GET['name'] + '.csv'

    feedbacks = Feedback.objects.filter(group__id=code)

    private_feedback = PrivateTestFeedback.objects.filter(feedback__in=feedbacks)
    public_feedback = PublicTestFeedback.objects.filter(feedback__in=feedbacks)
    semiprivate_feedback = SemiPrivateTestFeedback.objects.filter(feedback__in=feedbacks)

    generate_table(name, private_feedback, public_feedback, semiprivate_feedback)

    with open(name) as myfile:
        response = HttpResponse(myfile, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=' + name
        return response


def feedback_group_summary_as_csv(request):
    return feedback_group_as_csv(request, generate_table=generate_summary_table)
