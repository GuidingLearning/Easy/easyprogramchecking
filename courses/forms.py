from django import forms
from courses.models import Course
from users.models import User, Account


class CourseForm(forms.ModelForm):
    teachers = forms.ModelMultipleChoiceField(queryset=User.objects.none(), required=False)
    assistants = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False)

    class Meta:
        model = Course
        fields = ('name', 'code', 'teachers', 'assistants')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['code'].widget.attrs['class'] = 'form-control'
        self.fields['teachers'].widget.attrs['class'] = 'form-control multiselect'
        self.fields['assistants'].widget.attrs['class'] = 'form-control multiselect'

        self.fields['name'].widget.attrs['placeholder'] = 'Example Name'
        self.fields['code'].widget.attrs['placeholder'] = 'ER3443'

        self.fields['teachers'].queryset = User.objects.exclude(id=self.user.id)

        self.fields['teachers'].widget.attrs['autocomplete'] = 'off'
        self.fields['assistants'].widget.attrs['autocomplete'] = 'off'

    def save_teachers(self, object_id):
        """
        Add teachers

        :param object_id: ID of the course object.
        """
        teachers = self.cleaned_data['teachers']
        for teacher in teachers:
            obj = Course.objects.get(pk=object_id)
            teacher.account.teaching_courses.add(obj)

    def save_assistants(self, object_id):
        """
        Add assistants

        :param object_id: ID of the course object.
        """
        assistants = self.cleaned_data['assistants']
        for assistant in assistants:
            obj = Course.objects.get(pk=object_id)
            assistant.account.assistant_courses.add(obj)


class CourseEdit(forms.ModelForm):
    teachers = forms.ModelMultipleChoiceField(queryset=User.objects.none(), required=False)
    assistants = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False)

    current_teachers = forms.ModelMultipleChoiceField(queryset=User.objects.none(), required=False, widget=forms.CheckboxSelectMultiple)
    current_assistants = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False, widget=forms.CheckboxSelectMultiple)

    class Meta:
        model = Course
        fields = ('name', 'code', 'teachers', 'assistants', 'current_teachers', 'current_assistants')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.course = kwargs.pop('course', None)
        super().__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['code'].widget.attrs['class'] = 'form-control'

        self.fields['teachers'].widget.attrs['class'] = 'form-control multiselect'

        self.fields['assistants'].widget.attrs['class'] = 'form-control multiselect'

        self.fields['name'].widget.attrs['value'] = self.course.name
        self.fields['code'].widget.attrs['value'] = self.course.code

        self.fields['current_teachers'].queryset = Account.objects.filter(teaching_courses__id=self.course.id)
        self.fields['current_assistants'].queryset = Account.objects.filter(assistant_courses__id=self.course.id)

        self.fields['teachers'].queryset = Account.objects.exclude(teaching_courses__id=self.course.id)
        self.fields['assistants'].queryset = Account.objects.exclude(assistant_courses__id=self.course.id)

        self.fields['teachers'].widget.attrs['autocomplete'] = 'off'
        self.fields['assistants'].widget.attrs['autocomplete'] = 'off'

    def save_teachers(self, object_id):
        """
        Add teachers

        :param object_id: ID of the course object.
        """
        obj = Course.objects.get(pk=object_id)
        adding_teachers = self.cleaned_data['teachers']
        for teacher in adding_teachers:
            teacher.teaching_courses.add(obj)

        deleting_teachers = self.cleaned_data['current_teachers']

        for teacher in deleting_teachers:
            teacher.teaching_courses.remove(obj)

    def save_assistants(self, object_id):
        """
        Add assistants

        :param object_id: ID of the course object.
        """
        obj = Course.objects.get(pk=object_id)

        adding_assistants = self.cleaned_data['assistants']

        for assistant in adding_assistants:
            assistant.assistant_courses.add(obj)

        deleting_assistants = self.cleaned_data['current_assistants']

        for assistant in deleting_assistants:
            assistant.assistant_courses.remove(obj)

    def save_course(self, object_id):
        Course.objects.filter(pk=object_id).update(name=self.cleaned_data['name'], code=self.cleaned_data['code'])

    def save_all(self, object_id):

        if len(self.fields['current_teachers'].queryset) == len(self.cleaned_data['current_teachers']):
            return

        self.save_teachers(object_id)
        self.save_assistants(object_id)
        self.save_course(object_id)
