import os
import django

os.environ['DJANGO_SETTINGS_MODULE'] = 'easyprogramcheckin.settings'
django.setup()
from languages.models import Language
import subprocess
import shlex


class CompilationError(Exception):
    pass


class Script:

    def __init__(self, lang_name, submission_path):
        lang = Language.objects.get(name=lang_name)
        basename = os.path.basename(submission_path)
        formatter = {
            'file': basename,
            'efile': os.path.splitext(basename)[0]
        }
        self.compile_command = lang.compile_command.format(**formatter)
        self.run_command = lang.run_command.format(**formatter)
        self.working_dir = os.path.dirname(submission_path)

    def compile(self):
        """
        Compiles the program and raises a CompilationError exception on fail.
        :return: Compile exit code
        """
        if self.compile_command == '':
            return
        p = subprocess.Popen(shlex.split(self.compile_command),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=self.working_dir)
        outs, errs = p.communicate()
        if p.returncode != 0:
            raise CompilationError(errs.decode())

    def get_process(self):
        """
        Creates a new subprocess to run the script and returns it
        :return: Subprocess
        """
        return subprocess.Popen(shlex.split(self.run_command),
                                stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                cwd=self.working_dir)

    def run(self, test_input, timeout):
        """
        Runs a script with some input and a timeout in seconds and returns what's in stdout, an error code and if it was
        killed because of the timeout or compile error
        :param test_input: str
        :param timeout: int
        :return: str, str, bool
        """
        p = self.get_process()
        if test_input != '':
            p.stdin.write(test_input)

        killed_by_time = False
        try:
            out, err = p.communicate(timeout=timeout)
        except subprocess.TimeoutExpired:
            p.kill()
            out, err = p.communicate()
            killed_by_time = True

        return out, err, killed_by_time

    def process_error(self, err):
        # No Error
        if len(err) == 0:
            return 0
        # Error
        else:
            return 1
