"""
Compare scripts must receive two byte-strings and return True if they are equal, and False otherwise.
Remember to add your compare script to the compare_script_dict.
TODO: Maybe replace this with a CompareScript model in Django
"""
import sys, inspect

class CleanupScript:
    """
    Base class for compare script subclasses. Add a docstring to your subclasses describing
    the compare script.
    """
    @staticmethod
    def compare(a, b):
        """
        Expects bytes strings. Must be overridden.
        """
        raise NotImplementedError

    @staticmethod
    def get_name():
        """
        Returns the name of the compare script as a string. Must be overridden.
        """
        raise NotImplementedError

    @classmethod
    def __str__(cls):
        return cls.get_name()


class NoCleanup(CleanupScript):
    """
    Both strings must be an exact match, character by character.
    """
    @staticmethod
    def compare(a, b):
        return a == b

    @staticmethod
    def get_name():
        return 'No cleanup'


class WhitespaceCleaner(CleanupScript):
    """
    Removes trailing and leading whitespace, then, collapses consecutive whitespace and finally compares character
    by character. Tabulations, newlines and spaces are considered as whitespace.
    """
    @staticmethod
    def compare(a, b):
        whitespace = [ord(' '), ord('\n'), ord('\t')]
        n, m = len(a), len(b)
        # Ignore trailing whitespace
        while n > 0 and a[n-1] in whitespace:
            n -= 1
        while m > 0 and b[m-1] in whitespace:
            m -= 1
        i, j = 0, 0
        # Ignore leading whitespace
        while i < n and a[i] in whitespace:
            i += 1
        while j < m and b[j] in whitespace:
            j += 1
        # Compare each character. In case of a whitespace only substring, compare only the first whitespace character.
        while i < n and j < m:
            if a[i] != b[j]:
                return False
            while a[i] in whitespace:
                i += 1
            while b[j] in whitespace:
                j += 1
            i += 1
            j += 1
        if i == n and j == m:
            return True
        return False

    @staticmethod
    def get_name():
        return 'Whitespace cleanup'


# Get a list of all the classes in this file
cleanup_script_choices = [(name, obj.get_name()) for name, obj in inspect.getmembers(sys.modules[__name__])
                   if inspect.isclass(obj) and obj != CleanupScript]

