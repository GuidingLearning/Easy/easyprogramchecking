# Condiciones y metas del trabajo dirigido

## Contingencia
* Se debe de informar en caso de problemas
* Flexibilidad

## Objetivos
* Agregar a easy program checking un módulo para corregir tareas que tengan múltiples soluciones.
* Agregar a easy program checking un módulo para hacer clustering de las respuestas de la tarea.
    - El que escribe la tarea debe preocuparse de definir las categorias.
    - El motivo de este objetivo es poder hacer una revisión más uniforme según el tipo de error en la tarea asi como también identificar cuan frecuente es cada error.

		
## Milestones	
1. [X] Crear nuevo branch gitlab para agregar nuevas funcionalidades.
2. [X] Draft de manual de instalacion de easy program checking.
3. [ ] Crear lista de ejemplos de tareas con multiples soluciones correctas y scripts python que clasifican las respuestas. Ejemplos:
	- Partial order de cursos en curriculum (Topo sort).
	- Posicion de un elemento en arreglo desordenado.
	- ...
4. [ ] Modificar interfaz de EasyProgram Checking para permitir corregir:
	- Input output.
	- Lista de input + script de clasificacion (en python ?).
5. [ ] Modificar interfaz de EasyProgram Checking para permitir descarga de acuerdo a clasificacion.
(batch submission) no recuerdo este punto.
6. [ ] Merge branch trabajoDirigido con master.
7. [ ] Finalizar reporte.

## Condiciones de trabajo
* Reporte pdf mensual: no demasiado formal.
	Puede ser en tex o pdf.
	Reporte final debe incluir 3 propuestas de como se podria extender la funcionalidad.
* Trabajar en branch distinto de master (easy program checking).

* Reunion semanal:
	- Horario fijo: miercoles: 12:00
	- Si se cancelan 2 hay problemas (dar explicación)
	- Si no hay nada que reportar, enviar mensaje

# Metodología de evaluación
* Nota parcial a cada reporte, para dar feedback

# Equipo de desarrollo
* Sven participó en el equipo de desarrollo

Tomás Vallejos
