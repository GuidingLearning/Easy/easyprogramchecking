# EasyProgramChecking

## Resumen

Easy program checking es software orientado a la labor docente en el area de las ciencias de la computación.
Existen herramientas de evaluación automatizada que no necesitan intervención humana para entregar un resultado 
y en el otro extremo se encuentra el no utilizar herramientas y hacer revisiones manuales. En medio de estas se encuentra 
Easy Program Checking, un programa diseñado para dar feedback automatizado a los alumnos mientras realizan la tarea y una forma de evaluar
qué tipos de errores están cometiendo los estudiantes, de manera de facilitar la labor de corrección.

## Introducción

### Corrección automatizada
Easy Program Checking nace en un esfuerzo de generar ayudas para realizar evaluaciones y mejorar la enseñanza. Estamos interesados en que los alumnos hagan programas correctos, pero además que los hayan hecho bien, 
siguiendo buenas prácticas de diseño y programación. 
Un programa de evaluación automatizada sólo nos permite corroborar lo primero.
Por eso es necesario ser capaces de reconocer qué implementaciones son correctas dejandonos a los humanos la labor de distinguir
si están aplicando correctamente los conocimientos.

### Problemática con programas pasados
En el pasado se utilizaron Moulinette, Moulinette2 y Afeed intentando solucionar el mismo problema, estos
no fructiferaron por distintas, pero importantes razones: 
1. Para que sea utilizado debe tener una interfaz de usuario amigable.
2. Debe ser de fácil acceso (no requerir mucho esfuerzo del usuario para empezar a utilizarlo).

### Features deseables
En este trabajo dirigido se espera extender Easy Program Checking con el fin de que no solo pueda corroborar correctitud de
programas comparandolos con un output, si no que sea capaz de reconocer si una respuesta es correcta mediante 
la provision de un script del profesor. Además se espera que se puedan clasificar las tareas según los 
tipos de errores que presentan los alumnos para poder facilitar la corrección haciendo que las tareas del mismo
tipo las revise la misma persona.

### Trabajo dirigido y resultados del aprendizaje
Como resultados del aprendizaje se espera que el estudiante logre lo siguiente:
- Aprender uso de control de version via Git + Gitlab.
- Automatización parcial de la corrección de trabajos de alumnos, via el
software EasyProgramChecking.
- Aprender a manejar servidor y desplegar aplicación instalación con gran cantidad de usuarios (alumnos, auxiliares y profesores).

## Análisis de EasyProgramChecking a la fecha del 2020-03-01

- No hay documentacion.
- No hay manual de deploy.
- Fallas de seguridad.

Lo último se corresponde al sistema de defensa que tiene Easy Program Checking frente a scripts maliciosos
que pudieran enviar alguien externo. La única protección de Easy Program Checking es que corre en un usuario 
"nobody" de linux lo que quiere decir que tiene la mayoría de las llamadas a sistema restringidas, sin embargo
es posible enviar un script que haga shutdown, esto no apagara la máquina, pero Easy Program Checking dejará de 
funcionar y habrá que realizar un nuevo despliegue.

## Solución propuesta

- Agregar feature para corregir tareas con multiples soluciones
ejemplos.
- Agregar feature para hacer clusters de tareas según en que tests se equivocan y la razón del error.

## Solución realizada

1. Se realizan los cambios en la vista para poder adjuntar un script provisto por el profesor.    
2. Se realizan los cambios en el esquema de base de datos para permitir guardar el script provisto.
3. Se realiza una implementación no funcional del proceso de corrección para poder incorporar el script provisto para checkear la correctitud de la solución. Esto se puede encontrar en la rama td/semana9
4. Se realiza documentación sobre el proceso de testeo de easyprogramchecking y sobre el esquema de base de datos de easyprogramchecking. 
5. Se realiza un cambio al output csv de los feedback grupales para que sea más fácil catalogar los clusters

## Discusión

### Resumen del trabajo realizado

Se trabaja por milestones haciendo análisis de las posibildades para ejecutar cada una de ellas.

#### Milestone 1: Crear nuevo branch gitlab para agregar nuevas funcionalidades
* Se crea nueva rama 'trabajoDirigido'
* Se establecen las condiciones del trabajo dirigido conversadas en la reunión del día 15/04/2020

#### Milestone 2: Draft de manual de instalacion de easy program checking
* Reunión con Sven (equipo desarrollador) el día 16/04/2020: Se resuelven dudas sobre la instalación y preparación para easy program checking
* Se crea manual de instalación que contiene

    1. Instrucciones para desplegar easyprogramchecking (setup del proyecto Django)
    2. Instrucciones para crear un usuario administrador (setup de easyProgramChecking)
    3. Quickstart crear curso
    3. Quickstart crear tarea
    4. Documentacion tests 
    5. Ejemplos de tareas input output (sudoku y watermelon) hat tip a Sven
    6. Ejemplos de tests para tareas input output (sudoku y watermelon) hat tip a Sven
    3. Feature Bag
    3. To be fixed

#### Milestone 3: Crear lista de ejemplos de tareas con múltiples soluciones correctas.
* Se crea lista de ejemplos de tareas con múltiples soluciones correctas
* Se crean scripts python que clasifican las respuestas de dichas tareas

#### Milestone 4: Modificar interfaz de EasyProgram Checking para permitir corregir múltiple solution
* Cambios en el proyecto django
    * Cambios en el esquema de base de datos de assignment
    * Cambios en la vista de la página para crear assignment (falta actualizar la vista de editar assignment)
    * Cambios en el template para que mostrara una sección donde subir el script que debe checkear la solución
    
* Cambios en el sistema corrector  
  Se realiza un análisis del sistema corrector de easyprogramchecking y se descubre que los módulos están altamente acoplados, para proveer la funcionalidad requerida se debe realizar refactoring de 7 módulos:  
    * La vista submission de assignments
    * El cliente y servidor de script server communication
    * La tester factory, el scheduler de revisión, el scheduler slave y el tester
    
  La siguiente imagen que describe el proceso de easyprogramchecking al momento de enviar un script.
  ![image](images/Submission_proccess.png "submission proccess")

#### Milestone 5: Cambio de rumbo: se prioriza cambiar el output de un batch de submissions

* Se hacen cambios en el output csv para que sea más facil catalogar los clusters    
  El output antes:
  ![image](images/feedback_before.png "output antes")

  El output después:
  ![image](images/feedback_after.png "output después")

### Lineas de desarrollo futuro
Todo desarrollo futuro deberá incluir una semana para interiorizarse con el código.

1. Agregar feedback de usuario cuando se hace un batch submit    
    * Haber trabajado con proyectos django   
      1 semana si tiene experiencia en django, 2 si no

1. Cambiar la base de datos a postgres o alguna mas robusta que sqlite (sqlite3 no provee la funcionalidad distinct lo que complica el trabajo con la base de datos)
    * Realizar estudio de migraciónes  
      1 semana

    * Mejorar el modelo para "Feedbacks" y tipos de "Test"
      1 semana
    
    * Realizar la migración
      2-3 semanas (involucra estudio para conectar la nueva base de datos con django y dejar easyprogramchecking funcionando)

2. Dockerizar el proyecto    
    Esto incluye 2 items:
    * Dockerizar el proyecto django
      1-2 semanas para buscar una herramienta para containerizar el proyecto e interiorizar lo básico (recomendación: docker)

    * Generar una imagen y build para el proyecto
      1-2 semanas

    * Hacer que los scripts externos corran en containers
      2-3 semanas, involucra hacer que el scheduler cree nuevos containers en vez de nuevos threads.

3. Agregar una opción para que easyprogramchecking corrija tareas que tienen múltiples soluciones correctas.     

    * Se deben hacer los cambios en el modelo la vista y el controlador
      1 semana. También se puede utilizar la implementación provista en este trabajo dirigido ejecutando haciendo git revert de los commits "a6cb10ea83da7e42d3f6e530ba588591f843fbc8","7638e82e9fccbed7a7927ae3d4a698ae7c1554d5" y "d082453c36e40adfc4fce89bd9acb6af9f1949cb"
      
      Requiere hacer un refactor de 7 módulos.

    * La tester factory, el scheduler de revisión, el scheduler slave y el tester  
     1 semana (mirar cómo se intentó abordar en la rama td/semana9)
    
    * El cliente y servidor de script server communication.   
     1-2 semanas. El cliente y el servidor se comunican a través de sockets, se debe estudiar la forma en la que lo hace easyprogramchecking y enviar lo necesario desde el cliente para que el server pueda ocupar el script checker.

    * La vista submission de assignments     
     1 semana. Se debe cambiar la forma de interacción de la vista con el cliente para que se pueda utilizar lo implementado en los primeros 2 puntos.

## Apéndice 

### Modelo de datos para easyprogramchecking
Así se estructura el modelo de datos de easyprogramchecking

![image](images/DB-schema.png "Esquema de base de datos")

### Ejemplos de tareas de soluciones multiple
    * Partial order de cursos en curriculum (Topo sort).
    * Posicion de un elemento con múltiples ocurrencias en arreglo desordenado.
    * Camino más corto en un grafo (pueden haber múltiples caminos)
    * Encontrar el árbol cobertor de peso mínimo (múltiples óptimos).
    * Encontrar el óptimo orden de multiplicación en una cadena de matrices (múltiples óptimos)
    
### Ejemplos de scripts que chequeen correctitud.
#### Partial order de cursos

El input consiste en una serie de lineas donde el primer numero corresponde a un nodo y los siguientes corresponden a los requisitos para tomar ese curso

e.g

```
input valido

7
1
2
3 1 2
4 3
5 4
6 3
7 5 6

output valido

1
2
3
4
6
5
7
```

```python
def inputToGraph(intput):
    adj= {}
    lines = input.split("\n")
    for line in lines:
        line = line.split(" ")
        for i in range(len(line)):
            line[i] = int(line[i])

        node = line[0]
        restrictions = line[1:]

        adf[node] = restrictions

    return adj

def removeRestriction(adj,restriction):
    for node in adj.keys():
        adj[node] = adj[node].remove(restriction)
        
    return adj
    

def parse_input(input):
    i = input.split("$$")[0]
    o = input.split("$$")[1]
    return (i,o)


def check(input, output):
    adj = inputToGraph(input)
    lines = output.split("\n")
    for node in lines:
        if adj[node]:
            return -1
        else:
            adj = removeRestriction(node)

    return 1

def run():
    _input = input()
    (i,o) = parse_input(_input)
    return check(i,o)

run()

``` 
En este caso tenemos 2 grupos [-1,1].
- -1 Representa quienes toman un curso para el que no cumplen los requisitos
- 1 Representa quienes toman los cursos en un orden correcto

#### Posicion de un elemento en el arreglo
El input consiste en una lista de numeros separados por espacios, luego una coma y el numero que se quiere buscar

```
input valido

1 3 5 7 1 3, 1

output valido
4
```

```python
def parse_input(input):
    i = input.split("$$")[0]
    o = input.split("$$")[1]
    return (i,o)

def proccess_input(input):
    pair = input.split(", ")
    input = [int(i) for i in input.split(" ")]
    query = int(pair[1])
    return (input,query)

def check(input, output):
    (input,query) = proccessInput(input)

""" query is not in the input """
    if(output==-1):
        if(elem in input):
            return 0 
        else:
            return 1

""" query is in the input """
    if(input[output] == query):
        return 2
    else:
        return 3

def run():
    _input = input()
    (i,o) = parse_input(_input)
    return check(i,o)

run()


```
En este caso tenemos 4 grupos [-2,-1,1,2]. 
- -1 Representa a quienes responden que el elemento no esta cuando el elemento si estaba en el arreglo. 
- -2 Representa a quienes responden una posicion equivocada para una query
- 1 Representa a los que reconocen que un elemento efectivamente no esta en el arreglo
- 2 Representa a lo que encuentran correctamente la query en el arreglo




### Ejemplos de test cases

#### Formato del archivo
        El archivo debe contener un arreglo de diccionarios con 4 parametros:
        - Input (String)
        - Output (String)
        - Comment (String) (se mostrara en el feedback del test)
        - Test Type (String) de ["public", "semi_private", "private"] (private por defecto)

    Los comentarios se mostraran en el feedback del test y los tipos de test tienen los siguientes significados.

        - public: El feedback de este test sera un booleano indicando si el test paso, la salida esperada, la resultante y el comentario.
        - semi_private: El feedback de este test sera un booleano indicando si el test paso y el comentario.
        - private: Estos tests no son mostrados en el feedback.

        Estas especificaciones son solo para los feedbacks recibidos por los estudiantes, los asistentes y profesores veran todos los tests como si fueran publicos.

#### Ejemplos de archivo:

##### Json

```json
[
    {
        "Input": "[1,2,3,4]",
        "Output": "1",
        "Comment": "A Comment",
        "Test Type": "public"
    },
    {
        "Input": "[5,6,7,8]",
        "Output": "0",
        "Comment": "Another Coment",
        "Test Type": "private"
    }
]
```
##### Csv

    Input,Output,Comment,Test Type  
    "[1,2,3,4]",1,A Comment,public  
    "[5,6,7,8]",0,Another Coment,private  
    
##### Yml and Yaml
    
```yaml
- Input: "[1,2,3,4]"
Output: '1'
Comment: A Comment
Test Type: public
- Input: "[5,6,7,8]"
Output: '0'
Comment: Another Coment
Test Type: private
```


