
import json
l = []

for i in range(1,50):
	o = "NO"
	if i%2 == 0 and i!=2:
		o = "YES"
	o+="\n"
	dic = {
	"Input" : str(i),
	"Output": str(o),
	"Comment": "hola" + str(i),
	"Test Type": "public"
	}
	l += [dic]

f = open("testCases2.json", "w+")
f.write(json.dumps(l))