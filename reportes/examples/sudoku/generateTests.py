import json
lista = []

for i in range(1,26):
	input_dir = "sudokus_input/sudoku " + str(i) + ".txt"
	output_dir = "sudokus_output/sudoku " + str(i) + ".txt"
	sudoku_in = open(input_dir, "r")
	sudoku_out = open(output_dir, "r")
	visibility = "public"
	if(i%5==0):
		visibility = "private"
	elif(i%5-1==0):
		visibility = "semi_private"

	dic = {
		"Input" : sudoku_in.read(),
		"Output": sudoku_out.read() + '\n',
		"Comment": "hola" + str(i),
		"Test Type": visibility
		}
	lista += [dic]

f = open("testCases.json", "w+")
f.write(json.dumps(lista))