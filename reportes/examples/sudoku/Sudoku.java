import java.util.Scanner;
public class Sudoku{
    public static long operaciones = 0;
    public static int[][][] candidatos = new int[9][9][10];

    public static void descartar(int[][] sudoku){
        for(int i = 0; i<9; i++){
            for(int j = 0; j<9; j++){
                if(sudoku[i][j] > 0){
                    marcar(sudoku, i, j);
                }
            }
        }
    }

    public static int encontrarUnico(int[] arr){
        int x = -1;
        for (int i = 1; i < 10; i++) {
            if(0 == arr[i]){
                if(x!=-1)return -1;
                else{
                    x = i;
                }
            }
        }
        return x;
    }

    public static void rellenar(int[][] sudoku){
        boolean ans = false;
        for(int i = 0; i<9; i++){
            for(int j = 0; j<9; j++){
                if(sudoku[i][j] == 0){
                    int x = encontrarUnico(candidatos[i][j]);
                    if(x!=-1){
                        operaciones++;
                        sudoku[i][j] = x;
                        marcar(sudoku, i, j);
                        ans = true;
                    } 
                }
            }
        }
        if(ans)rellenar(sudoku);

    }
    

    public static void marcar (int[][] sudoku, int x, int y){
        int valor = sudoku[x][y];
        for (int i = 0; i < 9; i++) {
            candidatos[x][i][valor]++;
            candidatos[i][y][valor]++;
        }

        int fi = x - x%3;
        int ci = y - y%3;
        for (int i = fi; i < fi+3; i++) {
            for (int j = ci; j < ci+3; j++) {
                candidatos[i][j][valor]++;
            }
        }
    }

    public static void desmarcar(int[][] sudoku, int x, int y){
        int valor = sudoku[x][y];
        for (int i = 0; i < 9; i++) {
            candidatos[x][i][valor]--;
            candidatos[i][y][valor]--;
        }

        int fi = x - x%3;
        int ci = y - y%3;
        for (int i = fi; i < fi+3; i++) {
            for (int j = ci; j < ci+3; j++) {
                candidatos[i][j][valor]--;
            }
        }
    }
    
    public static int[][] leerSudoku() {
        Scanner s = new Scanner(System.in);
        //matriz[fila][columna]
        int[][] matriz = new int[9][9];
        //como el input siempre es igual, leemos 9 lineas
        for (int i = 0; i < 9; i++) {
        	//separamos cada linea en un arreglo de strings, con espacio como separador
            String[] linea = s.nextLine().split(" ");
            //leemos el arreglo de lineas
            for (int j = 0; j < linea.length; j++) {
            	//ponemos cada linea en la matriz
                matriz[i][j] = Integer.parseInt(linea[j]);
            }
        }
        s.close();
        return matriz;
    }

    public static boolean esLegal(int[][] sudoku, int fila, int columna, int n){
        for (int i = 0; i < 9; i++) {
            if(sudoku[fila][i] == n && i!=columna)return false;
            if(sudoku[i][columna] == n && i!=fila)return false;
        }

        int fi = fila - fila%3;
        int ci = columna - columna%3;
        for (int i = fi; i < fi+3; i++) {
            for (int j = ci; j < ci+3; j++) {
                if(sudoku[i][j]==n && (i!=fila || j!=columna))return false;
            }
        }

        return true;
    }

    public static boolean resolverSudoku(int[][] sudoku, int pos){
        if(pos>80)return true;
        int fila = pos/9;
        int col = pos%9;
        if(sudoku[fila][col]==0){
            for (int i = 1; i < 10; i++) {
                if(candidatos[fila][col][i] == 0){
                    sudoku[fila][col] = i;
                    marcar(sudoku, fila, col);
                    if(resolverSudoku(sudoku, pos+1)){
                        return true;
                    }
                    desmarcar(sudoku, fila, col);
                    sudoku[fila][col] = 0;
                }
            }
        }
        else{
            return resolverSudoku(sudoku, pos+1);
        }
        return false;
    }

    public static void printSudokuBonito(int[][] sudoku){
        for (int i = 0; i < sudoku.length; i++) {
            for (int j = 0; j < sudoku[0].length; j++) {
                if(j%3==0)System.out.print("| ");
                System.out.print(sudoku[i][j] + " ");
                if(j==8)System.out.print("|");
            }
            if(i%3-2==0){

                System.out.println("\n--------------------------");
            }else{
                System.out.println();
            }
        }
        System.out.println("operaciones: " + operaciones);
    }

    public static void printSudoku(int[][] sudoku){
        for (int i = 0; i < sudoku.length; i++) {
            for (int j = 0; j < sudoku[0].length; j++) {
                System.out.print(sudoku[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(operaciones);
    }

    public static boolean verificar(int[][] sudoku){
        for (int i = 0; i < sudoku.length; i++) {
            for (int j = 0; j < sudoku[0].length; j++) {
                if(!esLegal(sudoku, i, j, sudoku[i][j])){
                    System.out.println("sudoku malo");
                    return false;
                }
            }
        }
        System.out.println("sudoku bueno");
        return true;
    }

    public static void main(String[] args) {
        int[][] sudoku = leerSudoku();
        descartar(sudoku);
        rellenar(sudoku);
        resolverSudoku(sudoku, 0);
        verificar(sudoku);
        printSudoku(sudoku);

    }
}