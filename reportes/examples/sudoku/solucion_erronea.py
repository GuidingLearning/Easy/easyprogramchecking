
class Sudoku:
    def __init__(self, table):
        self.table = table
        self.operaciones = 0

        self.candidatos = [[[0 for _ in range(10)]for _ in range(9)] for _ in range(9)]
    


def leerSudoku():
	sudoku = []
	for i in range(9):
		line = input().split(' ')
		line2 = []
		for x in range(9):
			line2 += [int(line[x])]
		sudoku += [line2]
	return Sudoku(sudoku)



def descartar(sudoku):
    for i in range(9):
        for j in range(9):
            if sudoku.table[i][j] > 0:
                marcar(sudoku, i, j)
        
    
    


def encontrarUnico(arr):
    x = -1
    for i in range(1, 10):
        if 0 == arr[i]:
            if x!=-1:
            	return -1
            else:
                x = i
    return x

def rellenar(sudoku):
        ans = False
        for i in range(9):
            for j in range(9):
                if sudoku.table[i][j] == 0:
                    x = encontrarUnico(sudoku.candidatos[i][j])
                    if x!=-1:
                        sudoku.operaciones+=1
                        sudoku.table[i][j] = x
                        marcar(sudoku, i, j)
                        ans = True
        if ans:
        	rellenar(sudoku)

def marcar(sudoku, x, y):
    valor = sudoku.table[x][y]

    for i in range(9):
        sudoku.candidatos[x][i][valor] = sudoku.candidatos[x][i][valor]+1
        sudoku.candidatos[i][y][valor] = sudoku.candidatos[i][y][valor]+1

    fi = x - x%3
    ci = y - y%3
    for i in range(fi, fi+3):
        for j in range(ci, ci+3):
           sudoku.candidatos[i][j][valor] = sudoku.candidatos[i][j][valor]+1

    
      

def desmarcar(sudoku, x, y):
    valor = sudoku.table[x][y]
    for i in range(9):
        sudoku.candidatos[x][i][valor] = sudoku.candidatos[x][i][valor]-1
        sudoku.candidatos[i][y][valor] = sudoku.candidatos[i][y][valor]-1

    fi = x - x%3
    ci = y - y%3
    for i in range(fi, fi+3):
        for j in range(ci, ci+3):
            sudoku.candidatos[i][j][valor] = sudoku.candidatos[i][j][valor]-1




def esLegal(sudoku, fila, columna, n):
    for i in range(9):
        if sudoku.table[fila][i] == n and i!=columna:
        	return False
        if sudoku.table[i][columna] == n and i!=fila:
        	return False
    

    fi = fila - fila%3
    ci = columna - columna%3
    for i in range(fi, fi+3):
        for j in range(ci, ci+3):
            if sudoku.table[i][j]==n and (i!=fila or j!=columna):
            	return False
    return True

def resolverSudoku(sudoku, pos):
    if pos>80:
    	return True
    fila = int(pos/9)
    col = pos%9
    if(sudoku.table[fila][col]==0):
        for i in range(1, 10):
            if(sudoku.candidatos[fila][col][i] == 0):
                sudoku.table[fila][col] = i
                marcar(sudoku, fila, col)
                if(resolverSudoku(sudoku, pos+1)):
                    return True
                desmarcar(sudoku, fila, col)
                sudoku.table[fila][col] = 0
    else:
        return resolverSudoku(sudoku, pos+1)
    return False

def printSudoku(sudoku):
    for i in range(9):
        for j in range(9):
            print(sudoku.table[i][j], end=" ")
        print()
    print(sudoku.operaciones)


sudoku = leerSudoku()
descartar(sudoku)
rellenar(sudoku)
resolverSudoku(sudoku, 0)
sudoku.table[2][3]= 6
printSudoku(sudoku)