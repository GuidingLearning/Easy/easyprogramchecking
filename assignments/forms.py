from django import forms
from assignments.models import Assignment, Tag
from tempus_dominus.widgets import DateTimePicker
from django.db.models import Q


class AssignmentForm(forms.ModelForm):
    deadline = forms.DateTimeField(
        required=False,
        widget=DateTimePicker(
            options={
                'useCurrent': True,
                'collapse': False,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            },
        ),
    )

    course = forms.ModelChoiceField(queryset=None)

    class Meta:
        model = Assignment
        fields = ['title', 'allowed_languages', 'tags', 'cleanup_script', 'statement', 'source',
                  'course', 'deadline', 'period', 'published', 'tests']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

        teaching_courses = self.user.account.teaching_courses.all()
        assistant_courses = self.user.account.assistant_courses.all()
        self.fields['course'].queryset = (teaching_courses | assistant_courses).distinct()

        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['allowed_languages'].widget.attrs['class'] = 'w-100'
        self.fields['tags'].widget.attrs['class'] = 'w-100'
        self.fields['statement'].widget.attrs['class'] = 'form-control-file'
        self.fields['period'].widget.attrs['class'] = 'form-control'
        self.fields['source'].widget.attrs['class'] = 'form-control-file'
        self.fields['tests'].widget.attrs['class'] = 'form-control-file'

        self.fields['title'].widget.attrs['placeholder'] = 'Example Title'


class AssignmentEdit(forms.ModelForm):
    current_tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.none(), required=False,
                                                  widget=forms.CheckboxSelectMultiple)

    statement = forms.FileField(required=False)
    source = forms.FileField(required=False)
    tests = forms.FileField(required=False)

    class Meta:
        model = Assignment
        fields = ['title', 'allowed_languages', 'tags', 'current_tags', 'statement',
                  'deadline', 'published', 'period', 'source', 'tests']

    def __init__(self, *args, **kwargs):

        self.assignment = kwargs.pop('assignment', None)

        super().__init__(*args, **kwargs)

        self.fields['title'].widget.attrs['class'] = 'form-control'
        self.fields['title'].widget.attrs['value'] = self.assignment.title

        self.fields['allowed_languages'].widget.attrs['class'] = 'w-100'

        self.fields['tags'].widget.attrs['width'] = 'w-100'

        self.fields['period'].widget.attrs['class'] = 'form-control'
        self.fields['period'].widget.attrs['value'] = self.assignment.period

        self.fields['published'] = forms.BooleanField(initial=self.assignment.published, required=False)

        current_tags = self.assignment.tags.all()
        other_tags = Tag.objects.exclude(id__in=current_tags)

        self.fields['current_tags'] = forms.ModelMultipleChoiceField(queryset=current_tags, required=False,
                                                                     widget=forms.CheckboxSelectMultiple)

        self.fields['tags'] = forms.ModelMultipleChoiceField(queryset=other_tags, required=False)

        self.fields['deadline'] = forms.DateTimeField(
            required=False,
            widget=DateTimePicker(
                options={
                    'useCurrent': True,
                    'collapse': False,
                },
                attrs={
                    'append': 'fa fa-calendar',
                    'icon_toggle': True,
                }
            ), initial=self.assignment.deadline
        )

        self.fields['statement'].widget.attrs['class'] = 'form-control-file'

        self.fields['source'].widget.attrs['class'] = 'form-control-file'

        self.fields['tests'].widget.attrs['class'] = 'form-control-file'

    def save_all(self, object_id):

        obj = Assignment.objects.get(pk=object_id)

        obj.title = self.cleaned_data['title']
        obj.published = published = self.cleaned_data['published']
        obj.period = self.cleaned_data['period']
        obj.deadline = self.cleaned_data['deadline']

        new_tags = Tag.objects.filter(id__in=self.cleaned_data['tags'] | obj.tags.all())
        obj.tags.set(new_tags)
        obj.tags.set(obj.tags.exclude(id__in=self.cleaned_data['current_tags']))

        if self.cleaned_data['statement']:
            obj.statement = self.cleaned_data['statement']

        if self.cleaned_data['source']:
            obj.source = self.cleaned_data['source']

        if self.cleaned_data['tests']:
            obj.tests = self.cleaned_data['tests']

        obj.save()


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
