from django.contrib import admin
from assignments.models import Assignment, Tag

admin.site.register(Assignment)
admin.site.register(Tag)
