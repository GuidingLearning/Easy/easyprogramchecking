from django.urls import path

from assignments.views import AssignmentPageView, CreateAssignmentPageView, CreateTagView, EditAssignmentPageView

urlpatterns = [
    path('<int:pk>', AssignmentPageView.as_view(), name='assignment'),
    path('create_assignment', CreateAssignmentPageView.as_view(), name='create_assignment'),
    path('edit_assignment/<int:pk>', EditAssignmentPageView.as_view(), name='edit_assignment'),
    path('create_tag', CreateTagView.as_view(), name='create_tag')
]
