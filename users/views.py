from django.contrib.auth import authenticate, login
from django.shortcuts import reverse
from django.views.generic.edit import CreateView, UpdateView
from users.models import User
from users.forms import UserForm
from django.contrib import messages


class SignUpView(CreateView):
    template_name = 'registration/signup.html'
    model = User
    form_class = UserForm

    username = ""
    password = ""

    def form_valid(self, form):

        # save username and password for login
        self.username = form.cleaned_data['username']
        self.password = form.cleaned_data['password']

        return super(SignUpView, self).form_valid(form)

    def get_success_url(self):
        user = authenticate(username=self.username, password=self.password)
        if user is not None:
            if user.is_active:
                login(self.request, user)
        return reverse('landing_page')


class ProfileView(UpdateView):
    template_name = 'profile.html'
    model = User
    form_class = UserForm

    username = ""
    password = ""

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):

        # save username and password for login
        self.username = form.cleaned_data['username']
        self.password = form.cleaned_data['password']

        return super(ProfileView, self).form_valid(form)

    def get_success_url(self):
        user = authenticate(username=self.username, password=self.password)
        if user is not None:
            if user.is_active:
                login(self.request, user)
                messages.success(self.request, 'Profile Updated')
        return reverse('profile')
