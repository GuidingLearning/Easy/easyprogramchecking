from django.urls import path
from users.views import SignUpView, ProfileView
from users.forms import UserAuthenticationForm
from django.contrib.auth import views as viewsauth
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('signup/', SignUpView.as_view(extra_context={'title': 'Sign Up'}), name='signup'),
    path('login/', viewsauth.LoginView.as_view(template_name='registration/login.html',
                                               form_class=UserAuthenticationForm,
                                               extra_context={'title': 'Login'}),
         name='login'),
    path('profile/', ProfileView.as_view(extra_context={'title': 'Profile'}), name='profile'),
    path('logout/', viewsauth.LogoutView.as_view(), name='logout')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
